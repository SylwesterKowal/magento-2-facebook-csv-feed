<?php
declare(strict_types=1);

namespace Kowal\Facebook\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Store\Model\StoreManagerInterface;

use Kowal\Facebook\Helper\Data as FbHelper;
use Kowal\Facebook\Model\CronhistoryFactory;
use Kowal\Facebook\Model\Cronhistory;
use Kowal\Facebook\Logger\Logger;

class ExportFBFeed extends Command
{

    const STORE_ID = "name";
    const LINK_TYPE = "linkt_type";

    /**
     *
     * @var Filesystem
     */
    protected $filesystem;

    /**
     *
     * @var DirectoryList
     */
    protected $directorylist;

    /**
     *
     * @var FbHelper
     */
    protected $dataHelper;

    /**
     *
     * @var CronhistoryFactory
     */
    protected $cronhistoryFactory;

    /**
     *
     * @var DateTime
     */
    protected $date;

    /**
     *
     * @var Logger
     */
    protected $logger;


    /**
     * ExportFBFeed constructor.
     * @param Filesystem $filesystem
     * @param DirectoryList $directorylist
     * @param FbHelper $dataHelper
     * @param DateTime $date
     * @param Logger $logger
     * @param \Magento\Framework\App\State $state
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Filesystem $filesystem,
        DirectoryList $directorylist,
        FbHelper $dataHelper,
        DateTime $date,
        Logger $logger,
        \Magento\Framework\App\State $state,
        StoreManagerInterface $storeManager
    )
    {
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->directorylist = $directorylist;
        $this->dataHelper = $dataHelper;
        $this->date = $date;
        $this->logger = $logger;
        $this->state = $state;
        $this->storeManager = $storeManager;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

        $storeId = (empty($input->getArgument(self::STORE_ID))) ? 0 : $input->getArgument(self::STORE_ID);
        $link_redirect_to_checkout = (empty($input->getOption(self::LINK_TYPE))) ? 0 : 1;
        $output->writeln("START Store ID: " . $storeId);
        $feed_type = ($link_redirect_to_checkout) ? 'sklep' : "katalog";
        if ($this->dataHelper->isEnabled((int)$storeId)) {
            $this->logger->info('Feed process Start');
            $name = $this->getStoreCodeById((int)$storeId) . '_' . $storeId . '_fb_' . $feed_type;
            $filepath = 'fb' . DIRECTORY_SEPARATOR . $name . '.csv';
            $stream = $this->directory->openFile($filepath, 'w+');
            $stream->lock();
            $columns = $this->dataHelper->getDataHeader(Cronhistory::CRON);
            if ($columns) {
                foreach ($columns as $column) {
                    $header[] = $column;
                }
            } else {
                // log error
                return;
            }
            /* Write Header */
            $stream->writeCsv($header);
            $productdata = $this->dataHelper->getProductData($columns, $storeId, Cronhistory::CRON, $link_redirect_to_checkout);
            if ($productdata) {
                $size = 1;
                foreach ($productdata as $item) {
                    $stream->writeCsv($item);
                    $size++;
                }
            } else {
                // log error
                return;
            }
            $stream->lock();
            $stream->close();
            $this->logger->info('Feed process End');
        }
    }

    public function getStoreCodeById(int $id): ?string
    {
        try {
            $storeData = $this->storeManager->getStore($id);
            $storeCode = (string)$storeData->getCode();
        } catch (LocalizedException $localizedException) {
            $storeCode = null;
            $this->logger->error($localizedException->getMessage());
        }
        return $storeCode;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_facebook:exportfeed");
        $this->setDescription("Eksport danych do Katalogu Facebook");
        $this->setDefinition([
            new InputArgument(self::STORE_ID, InputArgument::OPTIONAL, "Store ID"),
            new InputOption(self::LINK_TYPE, "-s", InputOption::VALUE_NONE, "Sklep")
        ]);
        parent::configure();
    }
}

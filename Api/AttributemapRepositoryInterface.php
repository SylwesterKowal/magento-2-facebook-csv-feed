<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */
namespace Kowal\Facebook\Api;

/**
 * Facebook token repository interface.
 *
 * @api
 */
interface AttributemapRepositoryInterface
{
    /**
     * Lists facebook attribute mapping that match specified search criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria The search criteria.
     * @return \Kowal\Facebook\Api\Data\AttributemapSearchResultsInterface Facebook attribute search result interface.
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Loads a specified facebook map.
     *
     * @param int $entityId The attribute mapping entity ID.
     * @return \Kowal\Facebook\Api\Data\AttributemapInterface Attribute Mapping interface.
     */
    public function getById($entityId);

    /**
     * Delete Attribute Map
     *
     * @param \Kowal\Facebook\Api\Data\AttributemapInterface $attributemap
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Kowal\Facebook\Api\Data\AttributemapInterface $attributemap);
   
    /**
     * Deletes a specified attribute mapping.
     *
     * @param int $entityId The attribute mapping entity ID.
     * @return bool
     */
    public function deleteById($entityId);

    /**
     * Performs persist operations for a specified attribute mapping.
     *
     * @param \Kowal\Facebook\Api\Data\AttributemapInterface $attributemap.
     * @return \Kowal\Facebook\Api\Data\AttributemapInterface Facebook attribute map interface.
     * @since 100.1.0
     */
    public function save(Data\AttributemapInterface $attributemap);
}

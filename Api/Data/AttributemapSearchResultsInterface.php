<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */
namespace Kowal\Facebook\Api\Data;

/**
 * Gateway attribute mapping search result interface.
 *
 * @api
 * @since 100.1.0
 */
interface AttributemapSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Gets collection items.
     *
     * @return \Kowal\Facebook\Api\Data\AttributemapInterface[] Array of collection items.
     */
    public function getItems();

    /**
     * Sets collection items.
     *
     * @param \Kowal\Facebook\Api\Data\AttributemapInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

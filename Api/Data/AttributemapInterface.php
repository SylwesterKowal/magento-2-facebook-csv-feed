<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */
namespace Kowal\Facebook\Api\Data;

/**
 * Attribute Mapping interface.
 *
 * @api
 */
interface AttributemapInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID = 'mapping_id';
    const MAGE_ATTRIBUTE = 'mage_attribute';
    const FB_ATTRIBUTE = 'fb_attribute';
    
    /**
     * Get ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Kowal\Facebook\Api\Data\AttributemapInterface
     */
    public function setId($id);
    
    /**
     * Get MageAttribute.
     *
     * @return string|null
     */
    public function getMageAttribute();

    /**
     * Set MageAttribute.
     *
     * @param int $mageAttribute
     *
     * @return \Kowal\Facebook\Api\Data\AttributemapInterface
     */
    public function setMageAttribute($mageAttribute);
    
    /**
     * Get FbAttribute.
     *
     * @return string|null
     */
    public function getFbAttribute();

    /**
     * Set FbAttribute.
     *
     * @param string|null $fbattribute
     *
     * @return \Kowal\Facebook\Api\Data\AttributemapInterface
     */
    public function setFbAttribute($fbattribute);
}

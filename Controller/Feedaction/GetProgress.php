<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */

namespace Kowal\Facebook\Controller\Feedaction;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class GetProgress extends Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * GetProgress constructor.
     * @param Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param DirectoryList $directorylist
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        DirectoryList $directorylist
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->directorylist = $directorylist;
        parent::__construct($context);
    }

    public function execute()
    {
        $myFile = $this->directorylist->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'fb' . DIRECTORY_SEPARATOR . 'progress.txt';
        $data = 0;
        if (is_file($myFile)) {
            $data = (int)file_get_contents($myFile);
        }
        try {
            if ($data == '100') {
                unlink($myFile);
                $text = __("Feed Generated Successfully.");
                return $this->jsonResponse($text);
            }
            return $this->jsonResponse($data);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }



        try {
            $myFile = $this->directorylist->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'fb' . DIRECTORY_SEPARATOR . 'progress.txt';
            $data = 0;
            if (is_file($myFile)) {
                $data = 11; //(int)file_get_contents($myFile);
            }
            if ($data == '100') {
                unlink($myFile);
                $text = __("Feed Generated Successfully.");
                $this->getResponse()->setHeader('Content-type', 'text/html');
                $this->getResponse()->setBody($text);
                $this->getResponse()->sendResponse();
            }
            $this->getResponse()->setHeader('Content-type', 'text/html');
            $this->getResponse()->setBody(22);
            $this->getResponse()->sendResponse();

        } catch (Exception $ex) {


        }
    }

    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
    protected function _isAllowed()
    {
        return true;
    }
}

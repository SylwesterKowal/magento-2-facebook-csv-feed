<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */
namespace Kowal\Facebook\Controller\Adminhtml\Feedaction;

//use Magento\Framework\App\Filesystem\DirectoryList;

class Index extends \Magento\Backend\App\Action
{
    /**
     *
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context
    ) {
        parent::__construct($context);
    }
    public function _initAction()
    {
        $this->_view->loadLayout();
        return $this;
    }

    public function execute()
    {
        $this->_initAction()->_setActiveMenu(
            'Kowal_Facebook::facebook'
        )->_addBreadcrumb(
            __('Product Feed Action'),
            __('Product Feed Action')
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Product Feed Action'));
        $this->_view->renderLayout();
    }
    
    protected function _isAllowed()
    {
        return true;
    }
}

<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */

namespace Kowal\Facebook\Controller\Adminhtml\Feedaction;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;
use Kowal\Facebook\Helper\Data as FbHelper;
use Kowal\Facebook\Model\CronhistoryFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Kowal\Facebook\Model\Cronhistory;
use Kowal\Facebook\Logger\Logger;
use Magento\Store\Api\StoreRepositoryInterface;


class GenerateFeed extends \Magento\Backend\App\Action
{
    /**
     *
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     *
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     *
     * @var Filesystem
     */
    protected $filesystem;

    /**
     *
     * @var DirectoryList
     */
    protected $directorylist;

    /**
     *
     * @var FbHelper
     */
    protected $dataHelper;

    /**
     *
     * @var CronhistoryFactory
     */
    protected $cronhistoryFactory;

    /**
     *
     * @var DateTime
     */
    protected $date;

    /**
     *
     * @var Logger
     */
    protected $logger;

    /**
     * @var StoreRepositoryInterface
     */
    protected $_storeManager;

    /**
     * GenerateFeed constructor.
     * @param Context $context
     * @param FileFactory $fileFactory
     * @param Filesystem $filesystem
     * @param DirectoryList $directorylist
     * @param FbHelper $dataHelper
     * @param JsonFactory $resultJsonFactory
     * @param CronhistoryFactory $cronhistoryFactory
     * @param DateTime $date
     * @param Logger $logger
     * @param \Magento\Store\Api\StoreRepositoryInterface $storeManager
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        DirectoryList $directorylist,
        FbHelper $dataHelper,
        JsonFactory $resultJsonFactory,
        CronhistoryFactory $cronhistoryFactory,
        DateTime $date,
        Logger $logger,
        StoreRepositoryInterface $storeManager
    )
    {
        $this->_fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->directorylist = $directorylist;
        $this->dataHelper = $dataHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->cronhistoryFactory = $cronhistoryFactory;
        $this->date = $date;
        $this->logger = $logger;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $this->logger->info('Feed process Start');
            $progressfilepath = 'fb' . DIRECTORY_SEPARATOR . 'progress.txt';
            $progressfile = $this->directorylist->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'fb' . DIRECTORY_SEPARATOR . 'progress.txt';
            /* Open file */
            if (is_file($progressfile)) {
                unlink($progressfile);
            }
            $this->directory->openFile($progressfilepath, 'w+');
            $stringData = 0;

            file_put_contents($progressfile, $stringData);
            $storeManagerDataList = $this->_storeManager->getList();
            $datasize = count($storeManagerDataList);
            $size = 1;

            foreach ($storeManagerDataList as $store) {
                $temdata = file_get_contents($progressfile);
                shell_exec('/usr/local/bin/php /home/inspia/public_html/bin/magento kowal_facebook:exportfeed ' . $store->getId());
                $this->logger->info('progress katalog ' . $temdata . $store->getName() . $store->getCode());
                shell_exec('/usr/local/bin/php /home/inspia/public_html/bin/magento kowal_facebook:exportfeed ' . $store->getId() . ' -s');
                $this->logger->info('progress sklep ' . $temdata . $store->getName() . $store->getCode());
                sleep(2);
                $stringData = ($size * 100) / $datasize;
                file_put_contents($progressfile, $stringData);
                $size++;


            }

            $response['error'] = false;
            $resultJson = $this->resultJsonFactory->create();
            $resultJson->setData($response);
            $this->logger->info('Feed process End');
            return $resultJson;

        } catch (Exception $ex) {
            file_put_contents("__sklepy.txt", "cos poszło ni tak");
        }

    }

    public function getStoreParam()
    {
        return $this->getRequest()->getParam('store', 0);
    }

    protected function _isAllowed()
    {
        return true;
    }

    private function generate($progressfile)
    {
        $storeManagerDataList = $this->storeManager->getStores();
        $datasize = count($storeManagerDataList);
        $size = 1;
        foreach ($storeManagerDataList as $id => $store) {

            shell_exec('php bin/magento kowal_facebook:exportfeed ' . $id);
            $stringData = ($size * 100) / $datasize;
            $temdata = file_get_contents($progressfile);
            $this->logger->info('progress katalog ' . $temdata . $store['name'] . $store['code']);
            file_put_contents($progressfile, $stringData);
            sleep(2);

            shell_exec('php bin/magento kowal_facebook:exportfeed ' . $id . ' -s');
            $stringData = ($size * 100) / $datasize;
            $temdata = file_get_contents($progressfile);
            $this->logger->info('progress sklep ' . $temdata . $store['name'] . $store['code']);
            file_put_contents($progressfile, $stringData);
            $size++;
            sleep(2);
        }
    }
}

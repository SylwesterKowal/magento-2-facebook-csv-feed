<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */
namespace Kowal\Facebook\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;
use Kowal\Facebook\Model\Cronhistory;

/**
 * Class Options
 */
class CronHistoryStatus implements OptionSourceInterface
{

    protected $options;

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $optionArray = [Cronhistory::SUCCESS => Cronhistory::SUCCESS_LABEL,
                        Cronhistory::FAILED => Cronhistory::FAILED_LABEL
                      ];
        $res = [];
        if ($this->options === null) {
            foreach ($optionArray as $optionid => $optionValue) {
                $additional['value'] = $optionid;
                $additional['label'] = $optionValue;
                $res[] = $additional;
            }
            $this->options = $res;
            return $this->options;
        }
        return $this->options;
    }
}

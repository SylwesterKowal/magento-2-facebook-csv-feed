<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */

namespace Kowal\Facebook\Model\System\Config\Feed;

class Feedtype implements \Magento\Framework\Option\ArrayInterface
{

    const FeedXML = 'xml';
    const FeedCSV = 'csv';
    /**
     * Return feed type.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $feedtype = [
            ['value' => self::FeedCSV, 'label' => __('CSV')],
            ['value' => self::FeedXML, 'label' => __('XML')]
        ];
        return $feedtype;
    }
}

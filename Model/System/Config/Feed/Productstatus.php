<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */

namespace Kowal\Facebook\Model\System\Config\Feed;

class Productstatus implements \Magento\Framework\Option\ArrayInterface
{

    const APPROVED = 1;
    const PENDING = 2;
    /**
     * Return feed type.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $methods = [
            ['value' => self::APPROVED, 'label' => __('Approved')],
            ['value' => self::PENDING, 'label' => __('Pending')]
        ];
        return $methods;
    }
}

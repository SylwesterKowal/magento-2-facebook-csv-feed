<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */
namespace Kowal\Facebook\Cron;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Kowal\Facebook\Helper\Data as FbHelper;
use Kowal\Facebook\Model\CronhistoryFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Kowal\Facebook\Model\Cronhistory;
use Kowal\Facebook\Logger\Logger;

class GenerateFeed
{
    /**
     *
     * @var Filesystem
     */
    protected $filesystem;
    
    /**
     *
     * @var DirectoryList
     */
    protected $directorylist;
    
    /**
     *
     * @var FbHelper
     */
    protected $dataHelper;
    
     /**
      *
      * @var CronhistoryFactory
      */
    protected $cronhistoryFactory;
    
    /**
     *
     * @var DateTime
     */
    protected $date;
    
    /**
     *
     * @var Logger
     */
    protected $logger;
    
    /**
     *
     * @param Filesystem $filesystem
     * @param DirectoryList $directorylist
     * @param FbHelper $dataHelper
     */
    public function __construct(
        Filesystem $filesystem,
        DirectoryList $directorylist,
        FbHelper $dataHelper,
        CronhistoryFactory $cronhistoryFactory,
        DateTime $date,
        Logger $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->directorylist = $directorylist;
        $this->dataHelper = $dataHelper;
        $this->cronhistoryFactory = $cronhistoryFactory;
        $this->date = $date;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
    }
    
    public function execute()
    {

            $this->logger->info('Feed process Start');

            try {
                $this->logger->info('Feed process Start');

                $storeManagerDataList = $this->storeManager->getStores();

                foreach ($storeManagerDataList as $id => $store) {
                    if ($this->dataHelper->isEnabled($id) && $this->dataHelper->isCronEnabled($id)) {
                        shell_exec('php bin/magento kowal_facebook:exportfeed ' . $id);
                        shell_exec('php bin/magento kowal_facebook:exportfeed ' . $id . ' -s');
                    }
                }

                $this->logger->info('Feed process End');
                $cronHistoryModel = $this->cronhistoryFactory->create();
                $date = $this->date->gmtDate();
                $cronHistoryModel->setCronDate($date);
                $cronHistoryModel->setMessage(__("Feed Generated Successfully"));
                $cronHistoryModel->setType(Cronhistory::CRON);
                $cronHistoryModel->setStatus(Cronhistory::SUCCESS);
                $cronHistoryModel->save();

            } catch (Exception $ex) {
                $this->logger->info(__('Something went wrong.'));
            }



    }
}

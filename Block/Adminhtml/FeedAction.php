<?php
/**
 * Kowal
 * Copyright (C) 2019 Kowal <kontakt@kowal.co>
 *
 * @category Kowal
 * @package Kowal_Facebook
 * @copyright Copyright (c) 2019 Mage Delight (https://kowal.store/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Kowal <kontakt@kowal.co>
 */

namespace Kowal\Facebook\Block\Adminhtml;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Url as UrlHelper;
use Kowal\Facebook\Helper\Data as FbHelper;

class FeedAction extends \Magento\Backend\Block\Template
{

    /**
     *
     * @var UrlHelper
     */
    protected $urlHelper;

    /**
     *
     * @param Context $context
     */
    public function __construct(
        Context $context,
        UrlHelper $urlHelper,
        FbHelper $fbHelper
    )
    {
        parent::__construct($context);
        $this->storeManager = $context->getStoreManager();
        $this->urlHelper = $urlHelper;
        $this->fbHelper = $fbHelper;
    }

    protected function _prepareLayout()
    {
        $this->setChild(
            'feed_action_button',
            $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
                ->setData([
                    'label' => __('Generate Feed'),
                    'class' => 'assign icon-btn primary',
                    'on_click' => 'generateFeed()',
                ])
        );
        parent::_prepareLayout();
    }

    public function getFeedActionButtonHtml()
    {
        return $this->getChildHtml('feed_action_button');
    }

    public function getStoreParam()
    {
        return $this->getRequest()->getParam('store', null);
    }

    public function getDefaultStoreId()
    {
        foreach ($this->storeManager->getWebsites(false) as $_website):
            if ($_website->getIsDefault()) {
                return $_website->getDefaultStore()->getId();
            }
        endforeach;
    }

    public function getFeedUrl()
    {
        $storeId = $this->getStoreParam();
        $urlparams = ['_scope' => $storeId,
            '_nosid' => true,
        ];
        $baseUrl = $this->urlHelper->getUrl('', $urlparams);
        $feedUrl = $baseUrl . "pub" . "/" . "media" . "/" . "fb" . "/" . "fbshop.csv";
        return $feedUrl;
    }

    public function getFeedUrls()
    {
        $pub = ($this->fbHelper->isPubAsRoot()) ? "" : "pub".DIRECTORY_SEPARATOR;
        $storeManagerDataList = $this->storeManager->getStores();
        $sklepy = [];
        foreach ($storeManagerDataList as $id => $store) {
            $baseUrl = $this->urlHelper->getUrl('', ['_scope' => $id, '_nosid' => true,]);
            $feedUrl4Sklep = $baseUrl . $pub . "media" . DIRECTORY_SEPARATOR . "fb" . DIRECTORY_SEPARATOR . "{$store['code']}_{$id}_fb_sklep.csv";
            $feedUrl4Katalog = $baseUrl . $pub . "media" . DIRECTORY_SEPARATOR . "fb" . DIRECTORY_SEPARATOR . "{$store['code']}_{$id}_fb_katalog.csv";
            $sklepy[$store['code']] = ['name' => $store['code'],'id'=> $id, 'sklep' => $feedUrl4Sklep, 'katalog' => $feedUrl4Katalog];
        }

        return $sklepy;
    }

    public function getProgressUrl()
    {
        //$storeId = $this->getStoreParam();
//        $storeId = $this->getDefaultStoreId();
//        $urlparams = ['_scope' => $storeId,
//            '_nosid' => true,
//        ];
        $progressUrl = $this->urlHelper->getUrl('kowal_facebook/feedaction/GetProgress');
        return $progressUrl;
    }
}
